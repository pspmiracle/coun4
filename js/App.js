import GameScene from './controllers/GameScene';
import Controls from './controllers/ControlsCotnroller';
import Player from './controllers/PlayerController';
import Enemy from './controllers/EnemyController';
import Manager from './controllers/Manager';
import GamePlay from './controllers/GamePlay';

export default class GameApp {
	
	constructor(config){
		this.stage = new GameScene(config).getScene();
		this.originX = config.x;
		this.originY = config.y;	
		this.radius = config.radius;
		this.config = config;
	}
    
    load(){
        //add asset loader
        this.loader = PIXI.loader; // pixi exposes a premade instance for you to use.
        this.loader.add('grid',"images/column.png");
        this.loader.once('complete',this.init());
        this.loader.load();
    }
	
	init(){
		let manager = new Manager(this.config);
		new GamePlay(manager);
		
		let ballContainer = new PIXI.Container();
		this.gridContainer = new PIXI.Container();
        let controlsContainer = new PIXI.Container();
		
		this.stage.addChild(ballContainer);
		this.stage.addChild(this.gridContainer);
        this.stage.addChild(controlsContainer);
		
		this.player = new Enemy(manager, ballContainer);
		
		this.player = new Player(manager, ballContainer);
		this.player.addBall();
		
		this.controls = new Controls(manager, controlsContainer);
		this.controls.makeControls();
		
		this.makeGrid();
	}
	
	makeGrid(){
		var texture = PIXI.Texture.fromImage(this.loader.resources.grid.url);
		for (let n = 0; n < 7; n ++){
			let grid = new PIXI.Sprite(texture);
			grid.position.x = this.originX + (this.radius * 2) * n;
			grid.position.y = this.originY;
			this.gridContainer.addChild(grid);
		}
	}
	

	
}